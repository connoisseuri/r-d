import os

import numpy as np
import pandas as pd
from tensorflow.python.lib.io import file_io

from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import StratifiedShuffleSplit

def train_test_eq_split(X, y, n_per_class):
    sampled = X.groupby(y, sort=False).apply(
        lambda frame: frame.sample(n_per_class))
    mask = sampled.index.get_level_values(1)
    
    X_train = X.drop(mask)
    X_test = X.loc[mask]
    y_train = y.drop(mask)
    y_test = y.loc[mask]

    return X_train, X_test, y_train, y_test


def train_test_uneq_split(X, y, target, percent=0.1):
    # X_test = X.sample(frac=percent)
    # X_train = X.loc[~X.index.isin(X_test.index)]

    # use StratifiedShuffleSplit
    sss = StratifiedShuffleSplit(n_splits=1, test_size=percent, random_state=0)
    for train_index, test_index in sss.split(X, y):
        X_train, X_test = X.iloc[train_index], X.iloc[test_index]
    return X_train, X_test, X_train[target], X_test[target]


def get_metadata(data_dir, meta_data, target, ntop='_ALL_'):
    """
    Read Metadata file.
    :param data_dir: Folder directory
    :param meta_data: File name
    :param target: Target name
    :param ntop: Number of top levels to use
    :return:
    """
    with file_io.FileIO(os.path.join(data_dir, 'metadata', meta_data), mode='r') as f:
        metadata = pd.read_csv(f)
    metadata = metadata.set_index('Hash')
    metadata = metadata[[target]]
    metadata = metadata[~metadata[target].isnull()]

    # select only top categories
    if ntop != '_ALL_':
        top = metadata[target].value_counts()
        metadata = metadata.loc[metadata[target].isin(top.iloc[:ntop].index), :]

    return metadata


def get_features(data_dir, features_data):
    """
    Read features data
    :param data_dir: directory with data
    :param features_data: file with features
    :return: features data
    """
    with file_io.FileIO(os.path.join(data_dir, 'extracted_features', features_data), mode='rb') as f:
        features = pd.read_pickle(f)
    return features


def get_data(features, metadata, target, percent=0.1):
    """
    Separate the dataset on train and test; merge features with metadata
    :param features: data with features
    :param metadata: data with meta info
    :param target: Target name
    :param percent: percent of test dataset to split
    :return:
    """

    meta_features = metadata.join(features)
    meta_features = meta_features[pd.notnull(meta_features[1])]

    meta_features_train, meta_features_test, _, _ = train_test_uneq_split(meta_features, meta_features[target], target,
                                                                          percent)
    meta_features_train = meta_features_train.sample(frac=1)

    target_values = meta_features[target].values
    target_encoder = LabelBinarizer()
    target_encoder = target_encoder.fit(target_values)

    x_train = meta_features_train.drop([target], axis=1).values
    x_test = meta_features_test.drop([target], axis=1).values

    y_train = target_encoder.transform(meta_features_train[target].values)
    y_test = target_encoder.transform(meta_features_test[target].values)
    
    print('Data loaded')

    return x_train, y_train, x_test, y_test, meta_features_train, meta_features_test, target_encoder


def load_data(features_data, meta_data, target, ntop='_ALL_', data_dir='data/'):
    """
    Preprocess the data for modeling
    :param features_data: (str) filename with features
    :param meta_data: (str) filename with metadata
    :param target: (str) a target column
    :param ntop: (int/str) a number of top -N- categories to predict. if '_ALL_' we'll use all categories.
    :param data_dir: (str) path to the directory
    :return: x_train, y_train, x_test, y_test, features_train, features_test, styles_encoder
    """
    
    # if not target.startswith('CAT'):
    #     target = 'CAT ' + target

    metadata = get_metadata(data_dir, meta_data, target, ntop)

    features = get_features(data_dir, features_data)

    x_train, y_train, x_test, y_test, meta_features_train, meta_features_test, target_encoder = \
        get_data(features, metadata, target, percent=0.1)
    return x_train, y_train, x_test, y_test, meta_features_train, meta_features_test, target_encoder
