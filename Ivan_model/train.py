## How to run from the terminal with GPU:
# gcloud ml-engine jobs submit training most_awesome_job \
#     --job-dir JOB_DIR=gs://connoisseur_b1/dummy_job_dir \
#     --runtime-version 1.8 \
#     --module-name trainer.train \
#     --package-path ./trainer \
#     --region europe-west1 \
#     --config config.yaml \
#     -- \
#     --data-dir gs://connoisseur_b1/data \
#     --save-weights-dir gs://connoisseur_b1/model/Style/Style_baseline/exported_model \
#     --reports-dir gs://connoisseur_b1/model/Style/Style_baseline/act_vs_pred

## How to run from the terminal without GPU:
# the same as above without "--config config.yaml"

## How to run locally:
#gcloud ml-engine local train --job-dir ../modelling_test_styles --module-name trainer.train --package-path ./trainer -- --save-weights-dir weights


## How to deploy a model:
# Ivan Add here comments please

import os
import argparse
import numpy as np
import pandas as pd
from pandas.compat import StringIO

import keras.backend as K
from keras import optimizers
from keras import metrics
from keras.models import Sequential
from keras.callbacks import LearningRateScheduler, ModelCheckpoint
from keras.layers import BatchNormalization, Dense, Dropout, Activation
from keras.wrappers.scikit_learn import KerasClassifier

import tensorflow as tf
from tensorflow.python.lib.io import file_io
from tensorflow.python.saved_model import builder as saved_model_builder
from tensorflow.python.saved_model import tag_constants, signature_constants
from tensorflow.python.saved_model.signature_def_utils_impl import predict_signature_def

from sklearn.preprocessing import LabelBinarizer
from sklearn.utils import class_weight

from load_data import load_data
"""========================================================================================"""
import pandas as pd
from functools import reduce
from sklearn.preprocessing import MultiLabelBinarizer


def actual_vs_predicted(model, job_dir, x_train, x_test,  meta_features_train, meta_features_test, target_encoder,
                        target):
    # fill the tables and apply resulted model

    actulpred_test = pd.DataFrame({'Hash': meta_features_test.index,
                                   'y': meta_features_test[target]})

    actulpred_test['y_pred'] = target_encoder.inverse_transform(model.predict(x_test))

    # add probabilities
    for c in target_encoder.classes_:
        actulpred_test[c] = 0
    actulpred_test[target_encoder.classes_] = model.predict_proba(x_test)

    # save the results
    actulpred_test.to_csv('act_vs_pred_test.csv', index=False)
    move_file_to_bucket(file_name='act_vs_pred_test.csv', new_path=os.path.join(job_dir, 'act_vs_pred_test.csv'))

"""
def build_model(n_units1,
                n_units2,
                dropout1,
                dropout2,
                input_shape,
                output_shape):
    model = Sequential()
    model.add(BatchNormalization(input_shape=(input_shape,)))

    model.add(Dense(n_units1))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    if dropout1:
        model.add(Dropout(0.7))

    model.add(Dense(n_units2))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    if dropout2:
        model.add(Dropout(0.7))

    model.add(Dense(output_shape, activation='softmax'))

    optim = optimizers.Adam(lr=0)
    model.compile(loss='categorical_crossentropy',
                  optimizer=optim,
                  metrics=['accuracy'])
    return model
"""

def build_model(n_units1,
                n_units2,
                dropout1,
                dropout2,
                input_shape,
                output_shape):
    model = Sequential()
    model.add(BatchNormalization(input_shape=(input_shape,)))

    model.add(Dense(n_units1))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    if dropout1:
        model.add(Dropout(0.5))

    model.add(Dense(n_units2))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    if dropout2:
        model.add(Dropout(0.5))

    model.add(Dense(output_shape, activation='sigmoid')) # sigmoid to treat each output separately

    optim = optimizers.Adam(lr=0)
    model.compile(loss='binary_crossentropy', # binary cross-entropy to have a separate loss for each output
                  optimizer=optim,
                  metrics=['accuracy'])
    return model

def move_file_to_bucket(file_name, new_path):
    # the function to move files to our bucket
    file_io.copy(file_name, new_path, overwrite=True)
    file_io.delete_file(file_name)


def train_model(job_dir,
                save_weights_dir,
                reports_dir,
                target,
                features_data,
                meta_data,
                ntop='_ALL_',
                data_dir='data/',
                save_act_vs_pred=True,
                n_units1=2000,
                n_units2=2000,
                dropout1=True,
                dropout2=True,
                init_lr=1e-3):

    targets = ['CAT Style', 'CAT School', 'CAT Genre']
    ntops = ['_ALL_'] * 3

    #if not target.startswith('CAT'):
    #    target = target #'CAT ' + target

    #x_train, y_train, x_test, y_test, meta_features_train, meta_features_test, target_encoder = \
    #    load_data(features_data=features_data, meta_data=meta_data, target=target, ntop=ntop, data_dir=data_dir)

    loaded_data = []

    for target, ntop in zip(targets, ntops):
        _, _, _, _, meta_features_train, meta_features_test, _ = \
            load_data(features_data=features_data, meta_data=meta_data, target=target, ntop=ntop, data_dir=data_dir)
        loaded_data.append([meta_features_train, meta_features_test])

    #for i in range(1,3):
    #    loaded_data[i][0] = loaded_data[i][0][[targets[i]]]
    #    loaded_data[i][1] = loaded_data[i][1][[targets[i]]]

    merge_function = lambda df1, df2: df1.merge(df2, how='inner', left_index=True, right_index=True)

    merged_train_data = reduce(merge_function, [ld[0] for ld in loaded_data])
    merged_test_data = reduce(merge_function, [ld[1] for ld in loaded_data])

    x_train = merged_train_data.drop(targets, axis=1).values
    y_train = np.concatenate([MultiLabelBinarizer().fit_transform(merged_train_data[t].values.reshape(-1, 1))
                              for t in targets], axis=1)

    x_test = merged_test_data.drop(targets, axis=1).values
    y_test = np.concatenate([MultiLabelBinarizer().fit_transform(merged_test_data[t].values.reshape(-1, 1))
                              for t in targets], axis=1)

    # use subsample for testing
    """
    subset1 = 1000
    subset2 = 50

    x_train = x_train[:subset1]
    y_train = y_train[:subset1]
    x_test = x_train[:subset2]
    y_test = y_train[:subset2]
    meta_features_train = meta_features_train[:subset1]
    meta_features_test = meta_features_train[:subset2]
    """
    #print(meta_features_train.shape)

    # #####
    input_shape = x_train.shape[1]
    output_shape = y_train.shape[1]
    model = build_model(n_units1, n_units2, dropout1, dropout2, input_shape, output_shape)

    cw = class_weight.compute_class_weight('balanced',
                                           np.unique(np.argmax(y_train, axis=1)),
                                           np.argmax(y_train, axis=1))
    cw = {i:c for i, c in enumerate(cw)}

    lr_schedule = LearningRateScheduler(lambda epoch: init_lr / (2 ** (epoch // 20)))
    callbacks = [lr_schedule, ModelCheckpoint('best_model{}.h5'.format(target), monitor='val_acc', save_best_only=True)]

    """
    hist = model.fit(x_train, y_train,
                     validation_data=(x_test, y_test),
                     batch_size=32,
                     epochs=100, # replace by 200
                     callbacks=callbacks)
    """
    hist = model.fit(x_train, y_train,
                     batch_size=32,
                     epochs=100,  # replace by 200
                     callbacks=callbacks)

    move_file_to_bucket(file_name='best_model{}.h5'.format(target), new_path=os.path.join(save_weights_dir, 'weights.h5'))

    model_yaml = model.to_yaml()
    with open('model.yaml', 'w') as f:
        f.write(model_yaml)
    move_file_to_bucket('model.yaml', new_path=os.path.join(save_weights_dir, 'model.yaml'))

    #if save_act_vs_pred:
    #    actual_vs_predicted(model, reports_dir, x_train, x_test, meta_features_train, meta_features_test,
    #                        target_encoder, target)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
      '--job-dir',
      help='Cloud storage bucket to export the model and store temp files')
    parser.add_argument(
      '--data-dir',
      help='Path to the datasets')
    parser.add_argument(
      '--save-weights-dir',
      help='Where to save the trained weights')
    parser.add_argument(
      '--reports-dir',
      help='Where to save reports')
    parser.add_argument(
      '--target',
      help='Target variable (e.g. Style)')
    parser.add_argument(
      '--ntop',
      help='Number of top categories to keep', type=int)
    parser.add_argument(
      '--features-data',
      help='Features filename')
    parser.add_argument(
      '--meta-data',
      help='Metadata filename')
    args = parser.parse_args()
    arguments = args.__dict__
    #print(type(arguments))
    #1/0 508 for artists
    arguments = ['data/path_style', 'data', 'data/path_style', 'CAT Style', 'features.pkl', 'abt_2018-08-23.csv', '_ALL_', 'data', \
                'abt_2018-08-23.csv']
    train_model(*arguments)#**arguments)
#School loss: 0.4206 - acc: 0.8533 - val_loss: 0.9406 - val_acc: 0.7221
#Genre loss: 0.4259 - acc: 0.8496 - val_loss: 0.9268 - val_acc: 0.7237
#Artist loss: 0.3492 - acc: 0.8912 - val_loss: 1.3520 - val_acc: 0.7286
#Style
